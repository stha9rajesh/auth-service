package sejar.flightcenter.authservice.startup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import sejar.flightcenter.authservice.entity.OauthClientDetails;
import sejar.flightcenter.authservice.entity.Permission;
import sejar.flightcenter.authservice.entity.Role;
import sejar.flightcenter.authservice.entity.UserEntity;
import sejar.flightcenter.authservice.repository.ClientDetailsRepository;
import sejar.flightcenter.authservice.repository.PermissionRepository;
import sejar.flightcenter.authservice.repository.RoleRepository;
import sejar.flightcenter.authservice.repository.UserRepository;

import javax.annotation.PostConstruct;

@Component
public class CreateUser {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ClientDetailsRepository clientDetailsRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PermissionRepository permissionRepository;


    @PostConstruct
    public void createUser() {

        systemUser();
        systemoauthClientDetails();


    }

    private void systemoauthClientDetails() {
        OauthClientDetails clientDetails = new OauthClientDetails();
        String clientId = "mobile";
        clientDetails.setClientId(clientId);
        clientDetails.setClientSecret(passwordEncoder.encode("pin"));
        clientDetails.setWebServerRedirectUri("http://localhost:4200/login");
        clientDetails.setScope("write,read");
        clientDetails.setAccessTokenValidity(3600);
        clientDetails.setRefreshTokenValidity(1000);
        clientDetails.setResourceIds("inventory,payment");
        clientDetails.setAuthorizedGrantTypes("authorization_code,password,refresh_token,implicit");
        clientDetails.setAuthorities("");
        clientDetails.setAutoApprove("");
        clientDetails.setAdditionalInformation("{}");
        //clientDetails.setAdditionalInformation(Collections.<String, Object>singletonMap("foo", Arrays.asList("rab")));

        if (!clientDetailsRepository.findByClientId(clientId).isPresent()) {
            clientDetailsRepository.save(clientDetails);
        }

    }

    private void systemUser() {
        UserEntity user = new UserEntity();
        String username = "rajesh";
        user.setUserName(username);
        user.setPassword(passwordEncoder.encode("password"));
        user.setEmail("rajesh@gmail.com");
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnable(true);
        user.setVerificationCode("");
        //

        if (!userRepository.findByUserName(username).isPresent()) {
            user = userRepository.save(user);
            createRole(user);

        }
    }

    private Role createRole(UserEntity userEntity) {
        Role role = null;
        if (!roleRepository.findByName("admin").isPresent()) {
            role = new Role();
            role.setName("admin");
            role.setUserEntity(userEntity);
            role = roleRepository.save(role);
            createPermission(role);
        }
        return role;
    }

    private Permission createPermission(Role role) {
        Permission permission = null;
        if (
                (!permissionRepository.findByName("read,write").isPresent())
                        &&
                        (!permissionRepository.findByRole(role).isPresent())) {
            permission = new Permission();
            permission.setName("read,write");
            permission.setRole(role);
            permissionRepository.save(permission);
        }
        return permission;
    }


}

