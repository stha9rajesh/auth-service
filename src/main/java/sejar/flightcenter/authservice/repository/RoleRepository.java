package sejar.flightcenter.authservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import sejar.flightcenter.authservice.entity.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    @Query("SELECT r from Role r WHERE r.name=?1")
    Optional<Role> findByName(String role);
}
