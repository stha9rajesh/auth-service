package sejar.flightcenter.authservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sejar.flightcenter.authservice.entity.OauthClientDetails;

import java.util.Optional;

public interface ClientDetailsRepository extends JpaRepository<OauthClientDetails, String> {
    Optional<OauthClientDetails> findByClientId(String clientId);
}
