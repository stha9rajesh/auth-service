package sejar.flightcenter.authservice.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "user")
@Data
public class UserEntity implements Serializable {
    public UserEntity(){}

    public UserEntity(UserEntity userEntity) {

        this.userName = userEntity.getUserName();
        this.password   = userEntity.getPassword();
        this.email = userEntity.getEmail();
        this.enable = userEntity.isEnable();
        this.accountNonExpired = userEntity.isAccountNonExpired();
        this.accountNonLocked = userEntity.isAccountNonLocked();
        this.credentialsNonExpired = userEntity.isCredentialsNonExpired();
        this.roles = userEntity.getRoles();
        this.verificationCode = userEntity.getVerificationCode();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "username")
    private String userName;
    @Column(name = "password")
    private String password;
    @Column(name = "email")
    private String email;

    @Column(name = "enabled")
    private boolean enable;
    @Column(name = "accountNonExpired")
    private boolean accountNonExpired;
    @Column(name = "accountNonLocked")
    private boolean accountNonLocked;
    @Column(name = "credentialsNonExpired")
    private boolean credentialsNonExpired;
    @Column(name = "verification_code", nullable = true)
    private String verificationCode;

//    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinTable(name = "role_user", joinColumns = {
//          @JoinColumn(name = "user_id", referencedColumnName = "id")
//    }, inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
//    private List<Role> roles;

    @OneToMany(mappedBy = "userEntity", fetch = FetchType.EAGER)
    List<Role> roles;
}
