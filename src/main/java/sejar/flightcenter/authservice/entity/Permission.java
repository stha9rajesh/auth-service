package sejar.flightcenter.authservice.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "permission")
@Data
public class Permission extends BaseEntity {

    private String name;
//    @ManyToMany(mappedBy = "permission")
//    private List<Role> roles;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;


}
