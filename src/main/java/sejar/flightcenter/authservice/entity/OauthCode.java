package sejar.flightcenter.authservice.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "oauth_code")
@Data
public class OauthCode implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "bigint unsigned")
    private Integer id;

    @Column(unique = true,name = "code")
    private String code;

    @Lob
    @Column(name="authentication", columnDefinition = "mediumblob")
    private byte[] authentication;
}
