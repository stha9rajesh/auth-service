package sejar.flightcenter.authservice.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "role")
@Data
public class Role extends BaseEntity {

    private String name;
//    @ManyToMany(mappedBy = "roles")
//    private List<UserEntity> userEntitie;
//
//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(name = "permission_role", joinColumns = {
//            @JoinColumn(name = "role_id", referencedColumnName = "id")
//    }, inverseJoinColumns = {@JoinColumn(name = "permission_id", referencedColumnName = "id")})
//    private List<Permission> permission;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @OneToMany(mappedBy = "role",fetch = FetchType.EAGER)
    List<Permission> permission;


}
