package sejar.flightcenter.authservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import sejar.flightcenter.authservice.entity.Role;
import sejar.flightcenter.authservice.entity.UserEntity;
import sejar.flightcenter.authservice.repository.RoleRepository;
import sejar.flightcenter.authservice.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<Role> roles;
        Optional<UserEntity> user = userRepository.findByUserName(username);
        /**
         * Spring security create user details
         * we created userentity
         * convert userEntity to userDetails
         */
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        user.orElseThrow(() ->
                new UsernameNotFoundException("Username or password is worng!"));

        UserDetails userDetails = new AuthUserDetails(user.get());
        AccountStatusUserDetailsChecker checkUserDetails = new AccountStatusUserDetailsChecker();
        checkUserDetails.check(userDetails);
        return userDetails;
    }
}
