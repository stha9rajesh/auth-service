package sejar.flightcenter.authservice.service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import sejar.flightcenter.authservice.entity.UserEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class AuthUserDetails extends UserEntity implements UserDetails {
    public AuthUserDetails() {}
    public AuthUserDetails(UserEntity userEntity) {
        super(userEntity);
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authority = new ArrayList<>();
        getRoles().forEach(role -> {
            authority.add(new SimpleGrantedAuthority(role.getName()));
            role.getPermission().forEach(permisstion -> {
                authority.add(new SimpleGrantedAuthority(permisstion.getName()));
            });
        });
        return authority;
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public String getUsername() {
        return super.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return super.isAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return super.isAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return super.isCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled() {
        return super.isEnable();
    }
}
